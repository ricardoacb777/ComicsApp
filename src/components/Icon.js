import React from 'react';

export default ({ id }) => {
  const useTag = `<use xlink:href="/icons/icons.svg#icon-${id}" />`
  return <svg width={15} height={15} dangerouslySetInnerHTML={{ __html: useTag }} />
}