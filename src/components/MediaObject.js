import React from 'react'

export default ({name, thumb, url}) => {
    return (
        <div className="media mb-4">
            - &nbsp;
            {/* <img className="mr-3" src={thumb} alt={name} /> */}
            <div className="media-body">
                <h5 className="mt-0"><a rel="noopener noreferrer" target="_blank" href={url}>{name}</a></h5>
            </div>
        </div>
    )
}