import React from 'react'
import {
    Link
} from "react-router-dom"

export default ({ name, date, img, path}) => {
    return (
        <figure className="card-a">
            <div className="card-a__img">
                <Link to={path}>
                    <img src={img} alt="test" />
                </Link>
            </div>
            <figcaption className="card-a__caption">
                <h5>{ name }</h5>
                { date }
            </figcaption>
        </figure>
    )
}