import React from 'react'

export default () => {
    return (
        <div
            style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '50vh' }}>
            <div className="preloader"></div>
        </div>
    )
}