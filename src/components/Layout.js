import React from 'react'

export default ({ children}) => {
    return (
        <div>
            <header className="header">
                <h1>ComicBook</h1>
            </header>
            <div className="container">
                <main className="main">
                    {children}
                </main>
            </div>
        </div>
    )
}
