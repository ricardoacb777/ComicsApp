import React, { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import httpClient from '../api/client'
import MediaObject from '../components/MediaObject'
import Preloader from '../components/Preloader'


export default () => {
    let { id } = useParams()
    const [issue, setIssue] = useState(null)

    useEffect(() => {
        httpClient.get({ path: `issue/4000-${id}` }).then((data) => {
            setIssue(data.results)
        })
    }, [id])

    if (!issue) return  <Preloader />

    return (
        <div className="row">
            <div className="col-sm-12 col-md-6">
                <h3>Characters</h3>
                <hr/>
                {
                    Array.isArray(issue.character_credits) && issue.character_credits.map((credit, i) => (
                        <MediaObject
                            key={i}
                            name={credit.name}
                            thumb={issue.image.icon_url}
                            url={credit.site_detail_url}
                        />
                    ))
                }
                <br/>
                <h3>Teams</h3>
                <hr/>
                {
                    Array.isArray(issue.team_credits) && issue.team_credits.map((credit, i) => (
                        <MediaObject
                            key={i}
                            name={credit.name}
                            thumb={issue.image.icon_url}
                            url={credit.site_detail_url}
                        />
                    ))
                }
                <br/>
                <h3>Locations</h3>
                <hr/>
                {
                    Array.isArray(issue.location_credits) && issue.location_credits.map((credit, i) => (
                        <MediaObject
                            key={i}
                            name={credit.name}
                            thumb={issue.image.icon_url}
                            url={credit.site_detail_url}
                        />
                    ))
                }
                <br/>
                <h3>Concepts</h3>
                <hr/>
                {
                    Array.isArray(issue.concept_credits) && issue.concept_credits.map((credit, i) => (
                        <MediaObject
                            key={i}
                            name={credit.name}
                            thumb={issue.image.icon_url}
                            url={credit.site_detail_url}
                        />
                    ))
                }
            </div>
            <div className="col-sm-12 col-md-6">
                <img className="img-fluid" src={ issue.image?.original_url } alt="" />
            </div>
        </div>
    )
}