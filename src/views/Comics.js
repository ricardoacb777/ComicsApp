import React, { useState, useEffect } from 'react'
import classNames from 'classnames'
import httpClient from '../api/client'
import Layout from '../components/Layout'
import Issue from './Issue'
import Icon from '../components/Icon'
import Card from '../components/Card'
import Preloader from '../components/Preloader'
import {
	BrowserRouter as Router,
	Switch,
	Route
} from "react-router-dom";




export default () => {
	
	const [comics, setComics] = useState({
		results: []
	})
	const [isListView, setIsListView] = useState(false)
	
	useEffect(() => {
		httpClient.get({ path: 'issues', 'params[sort]': 'cover_date:desc' }).then((data) => {
			setComics(data)
		})
	}, [])

	if (!comics.results.length) return <Preloader />

	let gridClasnames = classNames({ grid: true, 'is-stacked': isListView})
	let actionViewListClasses = classNames({ btn: true, 'is-active': isListView })
	let actionViewGridClasses = classNames({ btn: true, 'is-active': !isListView })
	
	const MONTH_NAMES = ["January", "February", "March", "April", "May", "June",
		"July", "August", "September", "October", "November", "December"
	];

	const getFormatedDate = (dateString) => {
		const date = new Date(dateString)
		return `${MONTH_NAMES[date.getMonth()]} ${date.getDate()}, ${date.getFullYear()}`
	}

	return (
		<Router>
			<Layout>
				<Switch>
					<Route exact path="/">
						<div className="tile">
							<div className="tile__header">
								<span>Latest Issues</span>
								<div className="tile__actions">
									<button
										className={ actionViewListClasses }
										onClick={() => { setIsListView(true)}}
									>
										<Icon id='list' />
										List
									</button>
									<button
										className={ actionViewGridClasses }
										onClick={() => { setIsListView(false)}}
									>
										<Icon id='grid' />
										Grid
									</button>
								</div>
							</div>
							<div className={gridClasnames}>
								{ Array.isArray(comics.results) && (
										comics.results.map((comic, i) => (
											<div className="grid__cell" key={i}>
												<Card
													name={`${comic.name || comic.volume.name} #${comic.issue_number}`}
													date={getFormatedDate(comic.date_added)}
													path={`/issues/${comic.id}`}
													img={comic.image.small_url}
												/>
											</div>
										))
									)
								}
							</div>
						</div>
					</Route>
					<Route path={`/issues/:id`}><Issue /></Route>
				</Switch>
			</Layout>
		</Router>
	)
}