const ENDPOINT = 'https://us-central1-comicsapp-6ba0d.cloudfunctions.net/proxy'

export default {
    async get(params = null) {

        const responseLocal = localStorage.getItem(params.path)
        if (responseLocal) return JSON.parse(responseLocal)
        
        const response = await fetch(`${ENDPOINT}?${ params ? new URLSearchParams(params).toString() : '' }`)
        const data = await response.json()

        localStorage.setItem(params.path, JSON.stringify(data))
        return data
    }
}