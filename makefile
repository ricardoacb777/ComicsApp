# Initial variables
os ?= $(shell uname -s)

# Load custom setitngs
-include .env
export

ifdef command
override command := -c "$(command)"
endif

ifeq ($(os), Darwin)
open = open
else ifeq ($(os), Linux)
open = xdg-open
else ifeq ($(os), Windows_NT)
open = explorer
endif

install: | init test open # Boostrap the environment. Install, test and open the environment 

init:
	make build
	make up

build:
	@docker-compose build --force-rm

up:
	@docker-compose up -d --remove-orphans

stop:
	@docker-compose stop

down d: 
	docker-compose down $(service)

logs l: service ?= app
logs l: follow ?= -f
logs l: ## Show logs. Usage: make logs [service=app]
	docker-compose logs $(follow) $(service)

status ps s:
	docker-compose ps $(service)

open:
	xdg-open https://localhost:3000


copy: service ?= app
copy: path ?= /app/package.lock
copy: | up ## Copy app files/directories from service container to host
	docker cp $(shell docker-compose ps -q $(service)):$(path) .

sync: ## Copy composer files generated inside service container to host
	make copy path=/app/package-lock.json

publish: env ?= cdn
publish: | test release checknewrelease checkoutlatesttag ## 🎉  Publish new version to Production
	make deploy
	git checkout master

cli exec: service ?= app
cli exec: bash ?= ash
cli exec: tty ?= 
cli exec: ## Execute commands in service containers, use "command"  argument to send the command. By Default enter the shell.
	docker-compose exec $(tty) $(service) $(bash) $(command)

review: | test deploy ## Preview current branch in S3

deploy: env ?= review
deploy: | bundle sync.generated ## Preview current branch in S3
	make deploy.$(env)

test t: | testcafe ## Run tests

h help: ## ℹ️  This help.
	@echo 'ℹ️  Usage: make <task> [option=value]' 
	@echo 'Default task: init'
	@echo
	@echo '🛠️  Tasks:'
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z0-9., _-]+:.*?## / {printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := init
.PHONY: all test $(MAKECMDGOALS)
